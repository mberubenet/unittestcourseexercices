﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercice1
{
    public class CompteDeBanque
    {
        
        /// <summary>
        /// Solde du compte
        /// </summary>
        public decimal Solde {get; private set;}
        
        /// <summary>
        /// Création d'un compte sans solde initial
        /// </summary>
        public CompteDeBanque():this(0)
        {
            
        }

        /// <summary>
        /// Création d'un compte avec solde initial
        /// </summary>
        public CompteDeBanque(decimal soldeInitial)
        {
            Solde = soldeInitial;
        }

        /// <summary>
        /// Deposer des fonds dans le compte
        /// </summary>
        /// <param name="montant"></param>
        public void Deposer(decimal montant)
        {
            Solde += montant;
        }

        /// <summary>
        /// Retirer des fonds dans le compte
        /// </summary>
        /// <param name="montant"></param>
        public void Retirer(decimal montant)
        {
            if (montant > Solde)
            {
                throw new FondsInsuffisantsException(montant, Solde);
            }
            Solde -= montant;
        }
    }
}
