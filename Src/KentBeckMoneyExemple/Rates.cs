﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KentBeckMoneyExemple
{
    public class Rates
    {
        private readonly Dictionary<RatePair, decimal> _rates = new Dictionary<RatePair, decimal>();

        public void Add(Currency from, Currency to, decimal rate)
        {
            RatePair newPair = new RatePair(from, to);
            if(!_rates.ContainsKey(newPair))
            {
                _rates.Add(newPair, rate);
                //Add the reverse pair
                Add(to, from, (1 / rate));
            }
        }

        public decimal Rate(Currency from, Currency to)
        {
            if(from.Equals(to)) //same currency
            {
                return 1;
            }
            return _rates[new RatePair(from, to)];
        }

        public class RatePair
        {
            private readonly Currency _from;
            private readonly Currency _to;
            public RatePair(Currency from, Currency to)
            {
                _to = to;
                _from = from;
            }

            public override bool Equals(object obj)
            {
                RatePair other = obj as RatePair;
                return other != null &&
                    (this._from == other._from) &&
                    (this._to == other._to);
            }

            public override int GetHashCode()
            {
                return HashHelper.GenerateHash(_from, _to);
            }
        }
    }
}
