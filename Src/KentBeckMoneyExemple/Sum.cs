using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KentBeckMoneyExemple
{
    public class Sum:Expression
    {
        public Expression Augend { get; private set; }
        public Expression Addend { get; private set; }
        public Sum(Expression augend, Expression addend)
        {
            Augend = augend;
            Addend = addend;
        }

        public override Money Reduce(Rates rates,Currency to)
        {
            decimal amount = Augend.Reduce(rates, to).Amount + Addend.Reduce(rates, to).Amount;
            return new Money(amount, to);
        }

        public override Expression Times(int multiplier)
        {
            return new Sum(Augend.Times(multiplier), Addend.Times(multiplier));
        }
    }
}
