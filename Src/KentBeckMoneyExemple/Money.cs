﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KentBeckMoneyExemple
{
    public class Money:Expression
    {
        
        public Money(decimal amount, Currency currency)
        {
            Amount = amount;
            Currency = currency;
        }

        public decimal Amount { get; protected set; }

        public Currency Currency { get; protected set; }

        public override Expression Times(int multiplier)
        {
            return new Money(Amount * multiplier, Currency);
        }

        public override Money Reduce(Rates rates, Currency to)
        {
            decimal rate = rates.Rate(Currency, to);
            return new Money(Amount/rate, to);
        }

        public override bool Equals(object obj)
        {
            Money other = obj as Money;
            return other != null &&
                other.Currency == this.Currency &&
                other.Amount == this.Amount;
        }

        public override int GetHashCode()
        {
            return HashHelper.GenerateHash(Amount, Currency);
            
        }

        public static Money Dollar(int amount)
        {
            return new Money(amount,Currency.USD);
        }

        public static Money Franc(int amount)
        {
            return new Money(amount,Currency.CHF);
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", Amount, Currency);
        }
    }

}
