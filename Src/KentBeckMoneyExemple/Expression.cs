﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KentBeckMoneyExemple
{
    public abstract class Expression
    {
        public abstract Money Reduce(Rates rate,Currency to);

        public Expression Plus(Expression addend)
        {
            return new Sum(this, addend);
        }

        public abstract Expression Times(int multiplier);
    }
}
