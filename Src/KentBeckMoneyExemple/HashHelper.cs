﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KentBeckMoneyExemple
{
    public static class HashHelper
    {
        public static int GenerateHash(params object[] keyFields)
        {
            //See http://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-an-overridden-system-object-gethashcode for details
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                // Suitable nullity checks etc, of course :)
                foreach (var field in keyFields)
                {
                    hash = hash * 23 + (field == null ? 0 : field.GetHashCode());
                }
                return hash;
            }
        }
    }
}
