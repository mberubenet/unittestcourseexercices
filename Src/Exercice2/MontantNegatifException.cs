﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercice2
{
    public class MontantNegatifException:Exception
    {
        public MontantNegatifException(string action):base(string.Format("On ne peut utiliser un montant négatif pour {0}.",action))
        {
        
        }
    }
}
