﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercice2
{
    public class CompteDeBanque
    {
        /// <summary>
        /// Solde du compte
        /// </summary>
        public decimal Solde
        {
            get;
            private set;
        }

        /// <summary>
        /// Montant maximum de la ligne de crédit
        /// </summary>
        public decimal MargeDeCredit
        {
            get;
            private set;
        }
        /// <summary>
        /// Création d'un compte sans solde initial
        /// </summary>
        public CompteDeBanque():this(0)
        {
        }

        /// <summary>
        /// Création d'un compte avec solde initial
        /// </summary>
        public CompteDeBanque(decimal soldeInitial):this(soldeInitial,0)
        {
        }

        public CompteDeBanque(decimal soldeInitial,decimal margeDeCredit)
        {
            MargeDeCredit = margeDeCredit;
            if (soldeInitial + fondsDisponibles < 0)
            {
                throw new FondsInsuffisantsException(Math.Abs(soldeInitial), fondsDisponibles);
            }
            Solde = soldeInitial;
        }

        /// <summary>
        /// Deposer des fonds dans le compte
        /// </summary>
        /// <param name="montant"></param>
        public void Deposer(decimal montant)
        {
            if (montant < 0)
            {
                throw new MontantNegatifException("déposer");
            }
            Solde += montant;
        }

        /// <summary>
        /// Retirer des fonds dans le compte
        /// </summary>
        /// <param name="montant"></param>
        public void Retirer(decimal montant)
        {
            if (montant < 0)
            {
                throw new MontantNegatifException("retirer");
            }
            if (montant > fondsDisponibles)
            {
                throw new FondsInsuffisantsException(montant, fondsDisponibles);
            }
            Solde -= montant;
        }

        private decimal fondsDisponibles
        {
            get
            {
                return Solde + MargeDeCredit;
            }
        }
    }
}
