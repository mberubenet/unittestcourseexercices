﻿using System;

namespace Exercice2
{
    public class FondsInsuffisantsException:Exception
    {
        private readonly decimal _fondsDemandes;
        private readonly decimal _fondsDisponibles;

        public FondsInsuffisantsException(decimal fondsDemandes,decimal fondsDisponibles):
            base(string.Format("Impossible de retirer {0:c} car le solde disponible est {1:c}.",fondsDemandes,fondsDisponibles))
        {
            _fondsDemandes=fondsDemandes;
            _fondsDisponibles=fondsDisponibles;
        }
    }
}
