﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exercice1;

namespace Exercice1.Tests
{
    /// <summary>
    /// Pour cette exercice, implémentez les tests nécessaires pour vérifier les cas suivants
    /// </summary>
    [TestClass]
    public class CompteDeBanqueFixtures
    {
        /// <summary>
        /// Pour un compte de banque, le solde devrait être à 0
        /// </summary>
        [TestMethod]
        public void Creation_compte_sans_solde()
        {
            //Arrange
            var sut = new CompteDeBanque();
            //Act
            var result = sut.Solde;
            //Assert
            Assert.AreEqual(0, result);
        }

        /// <summary>
        /// Pour un compte de banque avec un montant initial, le solde devrait être égal à ce montant
        /// </summary>
        [TestMethod]
        public void Creation_compte_avec_solde()
        {
            //Arrange
            var sut = new CompteDeBanque(50);
            //Act
            var result = sut.Solde;
            //Assert
            Assert.AreEqual(50, result);
        }

        /// <summary>
        /// Pour un compte avec un solde a 0, si on fait un dépôt, le solde devrait être égale au montant du dépôt
        /// </summary>
        [TestMethod]
        public void Depot_compte_avec_solde_a_0()
        {
            //Arrange
            var sut = new CompteDeBanque(0);
            //Act
            sut.Deposer(100);
            //Assert
            Assert.AreEqual(100, sut.Solde);
        }

        /// <summary>
        /// Pour un compte avec un solde plus grand que 0 0, si on fait un dépôt, le solde devrait avoir augmenté du montant du dépôt
        /// </summary>
        [TestMethod]
        public void Depot_compte_avec_solde()
        {
            //Arrange
            var sut = new CompteDeBanque(50);
            //Act
            sut.Deposer(100);
            //Assert
            Assert.AreEqual(150, sut.Solde);
        }

        /// <summary>
        /// Pour un compte avec un solde, si on retire un montant plus petit ou égal au solde, le solde devrait être diminué du montant du retrait
        /// </summary>
        [TestMethod]
        public void Check_Withdraw_enough_fund()
        {
            //Arrange
            var sut = new CompteDeBanque(70);
            //Act
            sut.Retirer(25);
            //Assert
            Assert.AreEqual(45, sut.Solde);
        }

        /// <summary>
        /// Pour un compte avec un solde, si on retire un montant plus grand que le solde, une exception de type FondsInsuffisantsException doit être lancée
        /// </summary>
        [TestMethod]
        public void Check_Withdraw_not_enough_fund()
        {
            //Arrange
            var sut = new CompteDeBanque(50);
            //Act
            AssertThrows<FondsInsuffisantsException>(()=> sut.Retirer(125));
            //Assert
            
        }

        private static T AssertThrows<T>(Action codeExecute) where T : Exception
        {
            try
            {
                codeExecute();
            }
            catch (T e)
            {
                //Exception lancée. Ok. Si une exception d'un autre type est lancée, le test échoue
                return e;
            }
            Assert.Fail(string.Format("Exception de type {0} non lancée", typeof(T).Name));
            return null;
        }

    }
}
