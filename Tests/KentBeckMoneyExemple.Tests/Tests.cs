﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KentBeckMoneyExemple.Tests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void testMultiplication()
        {
            //Arrange
            Money five = Money.Dollar(5);
            //Act
            //Assert
            Assert.AreEqual(Money.Dollar(10), five.Times(2));
            Assert.AreEqual(Money.Dollar(15), five.Times(3));
        }

        [TestMethod]
        public void testEquality()
        {
            //Arrange
                
            //Act

            //Assert
            Assert.IsTrue(Money.Dollar(5).Equals(Money.Dollar(5)));
            Assert.IsFalse(Money.Dollar(5).Equals(Money.Dollar(6)));
            Assert.IsTrue(Money.Franc(5).Equals(Money.Franc(5)));
            Assert.IsFalse(Money.Franc(5).Equals(Money.Franc(6)));
            Assert.IsFalse(Money.Franc(5).Equals(Money.Dollar(5)));
        }

        [TestMethod]
        public void testFrancMultiplication()
        {
            //Arrange
            Money five = Money.Franc(5);
            //Act
            //Assert
            Assert.AreEqual(Money.Franc(10), five.Times(2));
            Assert.AreEqual(Money.Franc(15), five.Times(3));
        }

        [TestMethod]
        public void testCurrency()
        {
            //Arrange
            
            //Act

            //Assert
            Assert.AreEqual(Currency.USD, Money.Dollar(1).Currency);
            Assert.AreEqual(Currency.CHF, Money.Franc(1).Currency);
        }

        [TestMethod]
        public void testSimpleAddition()
        {
            //Arrange
            Money five = Money.Dollar(5);
            Expression sum = five.Plus(five);
            Rates rates = new Rates();
            //Act
            Money reduced = sum.Reduce(rates, Currency.USD);
            //Assert
            Assert.AreEqual(Money.Dollar(10), reduced);
        }

        [TestMethod]
        public void testPlusReturnsSum()
        {
            //Arrange
            Money five = Money.Dollar(5);
            Expression result = five.Plus(five);
            //Act
            Sum sum = (Sum)result;
            //Assert
            Assert.AreEqual(five, sum.Augend);
            Assert.AreEqual(five, sum.Addend);
        }

        [TestMethod]
        public void testReduceSum()
        {
            //Arrange
            Expression sum = new Sum(Money.Dollar(3), Money.Dollar(4));
            Rates rates = new Rates();
            //Act
            Money result = sum.Reduce(rates, Currency.USD);
            //Assert
            Assert.AreEqual(Money.Dollar(7), result);
        }

        [TestMethod]
        public void testReduceMoney()
        {
            //Arrange
            Rates rates = new Rates();
            //Act
            Money result = Money.Dollar(1).Reduce(rates, Currency.USD);
            //Assert
            Assert.AreEqual(Money.Dollar(1), result);

        }

        [TestMethod]
        public void testReduceMoneyDiffenrentCurrency()
        {
            //Arrange
            Rates rates = new Rates();
            rates.Add(Currency.CHF, Currency.USD, 2);
            //Act
            Money result = Money.Franc(2).Reduce(rates, Currency.USD);
            //Assert
            Assert.AreEqual(Money.Dollar(1), result);

        }

        [TestMethod]
        public void testPairEquals()
        {
            //Arrange
            Rates.RatePair pair1 = new Rates.RatePair(Currency.USD, Currency.CHF);
            Rates.RatePair pair2 = new Rates.RatePair(Currency.USD, Currency.CHF);
            Rates.RatePair pair3 = new Rates.RatePair(Currency.CHF, Currency.USD);
            //Act

            //Assert
            Assert.IsTrue(pair1.Equals(pair2));
            Assert.IsTrue(pair2.Equals(pair1));
            Assert.IsFalse(pair2.Equals(pair3));
            Assert.IsFalse(pair1.Equals(pair3));
        }

        [TestMethod]
        public void testIdentityRate()
        {
            //Arrange
            
            //Act

            //Assert
            Assert.AreEqual(1,(new Rates()).Rate(Currency.USD,Currency.USD));
        }

        [TestMethod]
        public void testMixedAddition()
        {
            //Arrange
            Expression fiveDollars = Money.Dollar(5);
            Expression tenFrancs = Money.Franc(10);
            Rates rates = new Rates();
            rates.Add(Currency.CHF, Currency.USD, 2);
            //Act
            Money result = fiveDollars.Plus(tenFrancs).Reduce(rates, Currency.USD);
            //Assert
            Assert.AreEqual(Money.Dollar(10), result);
        }

        [TestMethod]
        public void testMixedAdditionReverted()
        {
            //Arrange
            Expression fiveDollars = Money.Dollar(5);
            Expression tenFrancs = Money.Franc(10);
            Rates rates = new Rates();
            rates.Add(Currency.CHF, Currency.USD, 2);
            //Act
            Money result = fiveDollars.Plus(tenFrancs).Reduce(rates, Currency.CHF);
            //Assert
            Assert.AreEqual(Money.Franc(20), result);
        }

        [TestMethod]
        public void testSumPlusMoney()
        {
            //Arrange
            Expression fiveDollars = Money.Dollar(5);
            Expression tenFrancs = Money.Franc(10);
            Rates rates = new Rates();
            rates.Add(Currency.CHF, Currency.USD, 2);
            Expression sum = (new Sum(fiveDollars, tenFrancs)).Plus(fiveDollars);
            //Act
            Money result = sum.Reduce(rates, Currency.USD);
            //Assert
            Assert.AreEqual(Money.Dollar(15), result);
        }

        [TestMethod]
        public void testSumTimes()
        {
            //Arrange
            Expression fiveDollars = Money.Dollar(5);
            Expression tenFrancs = Money.Franc(10);
            Rates rates = new Rates();
            rates.Add(Currency.CHF, Currency.USD, 2);
            Expression sum = (new Sum(fiveDollars, tenFrancs)).Times(2);
            //Act
            Money result = sum.Reduce(rates, Currency.USD);
            //Assert
            Assert.AreEqual(Money.Dollar(20), result);
        }

    }
}
