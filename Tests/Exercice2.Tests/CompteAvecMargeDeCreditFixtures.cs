﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exercice2.Tests
{
    [TestClass]
    public class CompteAvecMargeDeCreditFixtures
    {
        //Modifier le compte de banque pour inclure une marge de 1000$.
        //Le solde initial peut-être négatif si il est à l'intérieur des limites de la marge de crédit
        //S'assurer que les retraits et dépôts fonctionne avec la marge de crédit
        //Ajouter également des validations supplémentaires pour valider qu'on ne dépose pas un montant négatif. Si la situation se produit, lancer une exception MontantNegatifException avec en paramètre l'action "deposer"
        //Ajouter également des validations supplémentaires pour valider qu'on ne retire pas un montant négatif. Si la situation se produit, lancer une exception MontantNegatifException avec en paramètre l'action "retirer"

        [TestMethod]
        public void Creation_CompteDeBanqueAvecMarge()
        {
            //Arrange
            var sut = new CompteDeBanque(0, 1000);
            //Act
            var result = sut.MargeDeCredit;
            //Assert
            Assert.AreEqual(1000, result);
        }

        [TestMethod]
        public void Creation_CompteDeBanqueAvecMarge_soldeNegatif_dans_les_limites()
        {
            //Arrange
            var sut = new CompteDeBanque(-500, 1000);
            //Act
            var result = sut.Solde;
            //Assert
            Assert.AreEqual(-500, result);
        }

        [TestMethod,ExpectedException(typeof(FondsInsuffisantsException))]
        public void Creation_CompteDeBanqueAvecMarge_soldeNegatif_hors_les_limites()
        {
            //Arrange
            var sut = new CompteDeBanque(-1000, 500);
            //Act
            var result = sut.Solde;
            //Assert
            //Expected exception
        }


        [TestMethod]
        public void Creation_CompteDeBanqueAvecMarge_soldePositif()
        {
            //Arrange
            var sut = new CompteDeBanque(500, 1000);
            //Act
            var result = sut.Solde;
            //Assert
            Assert.AreEqual(500, result);
        }

        [TestMethod]
        public void Depot_avec_solde_negatif()
        {
            //Arrange
            var sut = new CompteDeBanque(-500, 1000);
            //Act
            sut.Deposer(250);
            //Assert
            Assert.AreEqual(-250, sut.Solde);
        }

        [TestMethod]
        public void Retrait_avec_solde_negatif()
        {
            //Arrange
            var sut = new CompteDeBanque(-500, 1000);
            //Act
            sut.Retirer(250);
            //Assert
            Assert.AreEqual(-750, sut.Solde);
        }

        [TestMethod, ExpectedException(typeof(FondsInsuffisantsException))]
        public void Retrait_avec_solde_negatif_qui_depasse_la_limite()
        {
            //Arrange
            var sut = new CompteDeBanque(-500, 1000);
            //Act
            sut.Retirer(750);
            //Assert
            //Expected exception
        }

        [TestMethod]
        public void Retrait_avec_solde_positif_dans_la_limite()
        {
            //Arrange
            var sut = new CompteDeBanque(500, 1000);
            //Act
            sut.Retirer(750);
            //Assert
            Assert.AreEqual(-250, sut.Solde);
        }

        [TestMethod]
        public void Retrait_tout_le_solde_plus_marge()
        {
            //Arrange
            var sut = new CompteDeBanque(500, 1000);
            //Act
            sut.Retirer(1500);
            //Assert
            Assert.AreEqual(-1000, sut.Solde);
        }

        [TestMethod, ExpectedException(typeof(FondsInsuffisantsException))]
        public void Retrait_avec_solde_a_la_limite()
        {
            //Arrange
            var sut = new CompteDeBanque(-1000, 1000);
            //Act
            sut.Retirer(1);
            //Assert
        }

        [TestMethod, ExpectedException(typeof(FondsInsuffisantsException))]
        public void Retrait_avec_solde_positif_qui_depasse_la_limite()
        {
            //Arrange
            var sut = new CompteDeBanque(500, 1000);
            //Act
            sut.Retirer(1750);
            //Assert
            //Expected exception
        }

        [TestMethod] //Ne pas utiliser expected exception car on doit tester des paramètres de l'exception
        public void Depot_avec_montant_negatif()
        {
            //Arrange
            var sut = new CompteDeBanque(500, 1000);
            //Act
            var result = AssertThrows<MontantNegatifException>(()=>sut.Deposer(-1750));
            //Assert
            StringAssert.Contains(result.Message, "déposer");
        }

        [TestMethod] //Ne pas utiliser expected exception car on doit tester des paramètres de l'exception
        public void Retrait_avec_montant_negatif()
        {
            //Arrange
            var sut = new CompteDeBanque(500, 1000);
            //Act
            var result = AssertThrows<MontantNegatifException>(()=>sut.Retirer(-1750));
            //Assert
            StringAssert.Contains(result.Message, "retirer");
        }

        private static T AssertThrows<T>(Action action) where T:Exception
        {
            try
            {
                action();
            }
            catch(T ex)
            {
                //Ok, exception thrown
                return ex;
            }
            //Exception not thrown. Error
            Assert.Fail("Exception {0} expected but not thrown", typeof(T).Name);
            return null;
        }
    }
}
